//
//  AppDelegate.swift
//  CoreDataHW12
//
//  Created by Nazar Starantsov on 24.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.rootViewController = SearchViewController()
        self.window?.makeKeyAndVisible()
        
        return true
    }

   
}

