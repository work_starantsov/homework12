//
//  ImageEntity+CoreDataProperties.swift
//  CoreDataHW12
//
//  Created by Nazar Starantsov on 24.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//
//

import Foundation
import CoreData


extension ImageEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ImageEntity> {
        return NSFetchRequest<ImageEntity>(entityName: "ImageEntity")
    }

    @NSManaged public var imageData: Data?
    @NSManaged public var imageDescription: String?

}
