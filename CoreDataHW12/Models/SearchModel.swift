//
//  SearchModel.swift
//  SearchFlickr
//
//  Created by Nazar Starantsov on 16.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import CoreData

protocol SearchModelProtocol {
    var collectionDidChange: ((CollectionChangeStatus) -> ())? { get set }
    
    func getImages() -> [ImageEntity]
    func loadImages(by searchString: String)
    func clearImages()
}

class SearchModel: SearchModelProtocol {
    private var images: [ImageEntity] = []
    
    var collectionDidChange: ((CollectionChangeStatus) -> ())?
    var networkManager: NetworkManagerProtocol
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func clearImages() {
        images = []
        collectionDidChange?(.clearImages)
    }
    
    func getImages() -> [ImageEntity] {
        
        let fetchRequest: NSFetchRequest<ImageEntity> = ImageEntity.fetchRequest()
        
        do {
            let fetchedImages = try PersistentService.context.fetch(fetchRequest)
            self.images = fetchedImages
        } catch {}

        return images
    }
    
    func resetAllImages() {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = ImageEntity.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try PersistentService.context.execute(deleteRequest)
            try PersistentService.context.save()
        } catch {}
    }
    
    func loadImages(by searchString: String) {
        let url = API.searchPath(text: searchString, extras: "url_m")
        networkManager.getData(at: url) { data in
            guard let data = data else {
                
                self.collectionDidChange?(.errorWhileLoading)
                return
            }
            let responseDictionary = try? JSONSerialization.jsonObject(with: data, options: .init()) as? Dictionary<String, Any>
            
            guard let response = responseDictionary,
                let photosDictionary = response["photos"] as? Dictionary<String, Any>,
                var photosArray = photosDictionary["photo"] as? [[String: Any]] else {
                    self.collectionDidChange?(.errorWhileLoading)
                return
            }

            photosArray = photosArray.count > 20 ? photosArray.suffix(20) : photosArray
            
            let group = DispatchGroup()
            
            var imagesArray: [ImageEntity] = []
            self.resetAllImages()
            
            for object in photosArray {
                group.enter()
                let urlString = object["url_m"] as? String ?? ""
                let title = object["title"] as? String ?? ""
                
                if let url = URL(string: urlString) {
                    self.networkManager.getData(at: url) {imageData in
                        guard let data = imageData else {
                            group.leave()
                            return
                        }
                        
                        let image = ImageEntity(context: PersistentService.context)
                        image.imageData = data
                        image.imageDescription = title
                        PersistentService.saveContext()
                        imagesArray.append(image)
                        
                        group.leave()
                    }
                }
            }
            
            group.notify(queue: DispatchQueue.main) {
                self.images = imagesArray
                self.collectionDidChange?(self.images.count > 0 ? .foundImages : .noImages)
            }
        }
    }
}

enum CollectionChangeStatus {
    case foundImages
    case noImages
    case errorWhileLoading
    case clearImages
}
