//
//  SearchViewModel.swift
//  SearchFlickr
//
//  Created by Nazar Starantsov on 16.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

protocol SearchViewModelProtocol {
    var images: [ImageViewModelEntity] { get }
    
    var finishedUpdatingCollection: ((CollectionChangeStatus) -> ())? { get set }
    
    func searchImages(with string: String)
    func clearImages()
}

class SearchViewModel: SearchViewModelProtocol {

    var model: SearchModelProtocol {
        didSet {
            self.model.collectionDidChange = { (status) in
                self.finishedUpdatingCollection?(status)
            }
        }
    }
    
    var images: [ImageViewModelEntity] {
        get {
            var filteredImages = [ImageViewModelEntity]()
            
            let _ = self.model.getImages().filter {
                let imageData = $0.imageData ?? Data()
                let image: UIImage? = UIImage(data: imageData)
                if let safeImage = image {
                    filteredImages.append(
                        ImageViewModelEntity(description: $0.imageDescription ?? "", image: safeImage)
                    )
                    return true
                } else {
                    return false
                }
            }
            
            return filteredImages
        }
    }
    
    var finishedUpdatingCollection: ((CollectionChangeStatus) -> ())?
    var searchImagesWorkItem: DispatchWorkItem?
    
    init(model: SearchModel) {
        self.model = model
        defer {
            self.model = model
        }
    }
    
    func searchImages(with string: String) {
        if string.count > 2 {
            searchImagesWorkItem = DispatchWorkItem { [weak self] in
                guard let strongSelf = self else { return }
                
                if let searchImagesWorkItem = strongSelf.searchImagesWorkItem {
                    if searchImagesWorkItem.isCancelled { return }
                }
                
                strongSelf.model.loadImages(by: string)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: searchImagesWorkItem!)
            
        } else {
            clearImages()
        }
    }
    
    func clearImages() {
        model.clearImages()
    }
    
}
