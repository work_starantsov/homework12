//
//  NetworkManager.swift
//  SearchFlickr
//
//  Created by Nazar Starantsov on 16.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation

protocol NetworkManagerProtocol {
    func getData(at path: URL, completion: @escaping (Data?) -> Void)
}

class NetworkManager: NSObject, NetworkManagerProtocol {
    public static var shared = NetworkManager()
    
    private var session : URLSession!
    
    override init() {
        super.init()
        session = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    func getData(at path: URL, completion: @escaping (Data?) -> Void) {
        let dataTask = session.dataTask(with: path) { data, _, _ in
            completion(data)
        }
        dataTask.resume()
    }
}

extension NetworkManager: URLSessionDelegate {
    
}
