//
//  PersistentService.swift
//  CoreDataHW12
//
//  Created by Nazar Starantsov on 24.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import CoreData

class PersistentService {
    
    private init() {}
    
    // MARK: - Core Data stack
    static var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "CoreDataHW12")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    // MARK: - Core Data Saving support
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
